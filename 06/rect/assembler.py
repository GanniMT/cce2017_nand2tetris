import pathlib

# The Parser class
class Parser():
    def __init__(self, fileName, table):
        """ Initialises the `Parser` object

        Args:
            fileName (string): File name for the .asm file
            table (dictionary): An empty dictionary
        """
        self.file = open(fileName, "r")
        self.name = self.file.name
        self.table = {"SP": 0, "LCL":1 , "ARG": 2, "THIS": 3, "THAT": 4, "R8": 8, 
                      "R0": 0, "R1": 1, "R2": 2, "R3": 3, "R4": 4, "R5": 5, "R6": 6, "R7": 7,
                      "R9": 9, "R10": 10, "R11": 11, "R12": 12, "R13": 13, "R14": 14, "R15": 15,
                      "SCREEN": 16384, "KBD": 24576}
        self.otherTable = table
        self.num = 16

    def hasMoreCommands(self):
        """ Checks whether there are more commands in the input.

        Returns:
            string: Processed line from the `.asm` file, without comments
        """
        command = self.file.readline()
        if (command != "") and (command.strip() == ""):
            return "..."
        command = command.strip()
        index = command.find("//")
        if index == -1:
            pass
        elif index != 0:
            command = command[:index]
        command = command.strip()
        return command

    def commandType(self, command):
        """Checks the type of command on a particular `.asm` line - Used ONLY in
           the symbol-less version of the Assembler

        Args:
            command (string): Line from the `.asm` file

        Returns:
            string: Type of command (`A` for `A_COMMAND`, `C` for `C_COMMAND` and
            `L` for `L_COMMAND`) or comment if `COMMENT`
        """
        if command == "":
            return False
        elif command[0] == "@":
            return "A"
        elif command[0] == "(":
            return "L"
        elif (command[0].isupper()) or (command[0] == "0") or (command[0] == "1") or (command[0] == "-1"):
            return "C"
        else:
            return "COMMENT"
    
    def symbol(self, command):
        """Returns the symbol or decimal `Xxx` of the current command `@Xxx` or
        `(Xxx)`

        Args:
            command (string): Line from the `.asm` file

        Returns:
            string: Symbol or decimal `Xxx` of the current command `@Xxx` or `(Xxx)`
        """
        command = command.strip("@")
        if command.isdigit():
            digit = int(command)
            binary = bin(digit)
        elif command in self.table:
            digit = self.table[command]
            binary = bin(digit)
        elif command in self.otherTable:
            digit = self.otherTable[command]
            binary = bin(digit)
        else:
            binary = bin(self.num)
            self.otherTable[command] = self.num
            self.num += 1
        result = (binary[2:]).zfill(16)
        return result
    
    def dest(self, command):
        """Returns the `dest` mnemonic in the C-command

        Args:
            command (string): Line from the `.asm` file

        Returns:
            string: The `dest` mnemonic in the C-command
        """
        index = command.find("=")
        if index == -1:
            return ""
        destination = command[:index]
        return destination

    def comp(self, command):
        """Returns the `comp` mnemonic in the C-command

        Args:
            command (string): Line from the `.asm` file

        Returns:
            string: The `comp` mnemonic in the C-command
        """
        indexEqual = command.find("=")
        indexSColon = command.find(";")
        if indexSColon == -1:
            compare = command[(indexEqual + 1):]
        else:
            compare = command[(indexEqual + 1):indexSColon]
        return compare
    
    def jump(self, command):
        """Returns the `jump` mnemonic in the C-command

        Args:
            command (string): Line from the `.asm` file

        Returns:
            string: The `jump` mnemonic in the C-command
        """
        indexSColon = command.find(";")
        if indexSColon == -1:
            jump = ""
        else:
            jump = command[(indexSColon + 1):]
        
        return jump

# The Code class
class Code():
    def __init__(self):
        """Initialises the `Code` object
        """
        pass

    def dest(self, destination):
        """Returns the binary code of the `dest` mnemonic

        Args:
            destination (string): The `dest` mnemonic

        Returns:
            string: The 3-bit binary code of the `dest` mnemonic
        """
        if (destination == "") or (destination == "null"):
            value = "000"
        elif destination == "M":
            value = "001"
        elif destination == "D":
            value = "010"
        elif destination == "MD":
            value = "011"
        elif destination == "A":
            value = "100"
        elif destination == "AM":
            value = "101"
        elif destination == "AD":
            value = "110"
        elif destination == "AMD":
            value = "111"
        else:
            value = "INVALID"
        return value

    def comp(self, compare):
        """Returns the binary code of the `comp` mnemonic

        Args:
            compare (string): The `comp` mnemonic

        Returns:
            string: The 7-bit binary code of the `comp` mnemonic
        """
        if compare == "0":
            value = "0101010"
        elif compare == "1":
            value = "0111111"
        elif compare == "-1":
            value = "0111010"
        elif compare == "D":
            value = "0001100"
        elif compare == "A":
            value = "0110000"
        elif compare == "!D":
            value = "0001101"
        elif compare == "!A":
            value = "0110001"
        elif compare == "-D":
            value = "0001111"
        elif compare == "-A":
            value = "0110011"
        elif compare == "D+1":
            value = "0011111"
        elif compare == "A+1":
            value = "0110111"
        elif compare == "D-1":
            value = "0001110"
        elif compare == "A-1":
            value = "0110010"
        elif (compare == "D+A") or (compare == "A+D"):
            value = "0000010"
        elif compare == "D-A":
            value = "0010011"
        elif compare == "A-D":
            value = "0000111"
        elif (compare == "D&A") or (compare == "A&D"):
            value = "0000000"
        elif (compare == "D|A") or (compare == "A|D"):
            value = "0010101"
        elif compare == "M":
            value = "1110000"
        elif compare == "!M":
            value = "1110001"
        elif compare == "-M":
            value = "1110011"
        elif compare == "M+1":
            value = "1110111"
        elif compare == "M-1":
            value = "1110010"
        elif (compare == "D+M") or (compare == "M+D"):
            value = "1000010"
        elif compare == "D-M":
            value = "1010011"
        elif compare == "M-D":
            value = "1000111"
        elif (compare == "D&M") or (compare == "M&D"):
            value = "1000000"
        elif (compare == "D|M") or (compare == "M|D"):
            value = "1010101"
        else:
            value = "INVALID"
        return value

    def jump(self, jump):
        """Returns the binary code of the `jump` mnemonic

        Args:
            jump (string): The `jump` mnemonic

        Returns:
            string: The 3-bit binary code of the `jump` mnemonic
        """
        if (jump == "") or (jump == "null"):
            value = "000"
        elif jump == "JGT":
            value = "001"
        elif jump == "JEQ":
            value = "010"
        elif jump == "JGE":
            value = "011"
        elif jump == "JLT":
            value = "100"
        elif jump == "JNE":
            value = "101"
        elif jump == "JLE":
            value = "110"
        elif jump == "JMP":
            value = "111"
        else:
            value = "INVALID"
        return value

# The Symbol Table class
class SymbolTable():
    def __init__(self, fileName):
        """Initialises the `SymbolTable` object

        Args:
            fileName (string): File name for the .asm file
        """
        self.file = open(fileName, "r")
        self.name = self.file.name
        self.table = {}
        self.lines = 0
    
    def record(self, command):
        """Takes note of an `L_COMMAND`

        Args:
            command (string): Line from the `.asm` file

        Returns:
            boolean: True if there are brackets, False if there are not
        """
        openBracket = command.find("(")
        closeBracket = command.find(")")
        if (openBracket == -1) or (closeBracket == -1):
            return False
        symbol = command[(openBracket + 1) : closeBracket]
        self.table[symbol] = self.lines
        return True

    def readNewLine(self):
        """Reads a new line from the `.asm` file and cleans it

        Returns:
            string: Cleaned line from `.asm` line
        """
        command = self.file.readline()
        if (command != "") and (command.strip() == ""):
            return "..."
        command = command.strip()
        commentIndex = command.find("//")
        if commentIndex == -1:
            pass
        elif commentIndex != 0:
            command = command[:commentIndex]
        command = command.strip()
        return command

    def commandType(self, command):
        """Checks the type of command on a line

        Args:
            command (string): Line from the `.asm` file

        Returns:
            string: Type of command (`A` for `A_COMMAND`, `C` for `C_COMMAND` and
            `L` for `L_COMMAND`) or comment if `COMMENT`
        """
        if command == "":
            return False
        elif command [0] == "@":
            return "A"
        elif command[0] == "(":
            return "L"
        elif (command[0].isupper()) or (command[0] == "0") or (command[0] == "1") or (command[0] == "-1"):
            return "C"
        else:
            return "COMMENT"
        
    def processLine(self):
        """Processes a line from the .asm line

        Returns:
            boolean: True if line or command is processed, False if unknown line
        """
        command = self.readNewLine()
        commandType = self.commandType(command)
        if commandType == "L":
            self.record(command)
            return True
        elif (commandType == "A") or (commandType == "C"):
            self.lines += 1
            return True
        elif commandType == "COMMENT":
            return True
        else:
            return False

# The Assembler class
class Assembler():
    def __init__(self, command, commandType):
        """Initialises the `Assembler` object

        Args:
            command (string): Processed `.asm` line
            commandType (string): Type of command
        """
        self.command = command
        self.commandType = commandType
    
    def processType(self, command, commandType, parser):
        """Returns the binary instruction, depending on the type of command

        Args:
            command (string): Line from the `.asm` file
            commandType (string): Type of command
            parser (object): `Parser` object

        Returns:
            strings: `destCode`, `compCode`, `jumpCode` and `address`
        """
        codeTranslate = Code()
        if commandType == "C":
            destination = parser.dest(command)
            destCode = codeTranslate.dest(destination)
            compare = parser.comp(command)
            compCode = codeTranslate.comp(compare)
            jump = parser.jump(command)
            jumpCode = codeTranslate.jump(jump)
            address = ""
        elif commandType == "A":
            destCode = ""
            compCode = ""
            jumpCode = ""
            address = parser.symbol(command)
        else:
            destCode = ""
            compCode = ""
            jumpCode = ""
            address = ""
        
        return destCode, compCode, jumpCode, address

    def write(self, compute, address, file):
        """Writes binary to the `.hack` file

        Args:
            compute (string): compCode
            address (string): Address
            file (object): File object
        """
        if compute != "":
            compute = "111" + compute
        if compute != "":
            file.write(compute + "\n")
        elif address != "":
            file.write(address + "\n")
        else:
            pass

if __name__ == "__main__":
    asmFileInput = input("Enter .asm file: ")

    asmFile = asmFileInput + ".asm"
    hackFile = asmFileInput + ".hack"

    asmPath = pathlib.Path(asmFile)

    if asmPath.is_file():
        preProcess = SymbolTable(asmFile)
        while (True):
            if preProcess.processLine():
                pass
            else:
                break

        table = preProcess.table
        preProcess.file.close()

        hack = open(hackFile, "w+")
        file = Parser(asmFile, table)
        more = file.hasMoreCommands()
        while more != "":
            commandType = file.commandType(more)
            assembler = Assembler(more, commandType)
            destination, compare, jump, address = assembler.processType(more, commandType, file)
            compute = compare + destination + jump
            assembler.write(compute, address, hack)
            more = file.hasMoreCommands()
        hack.seek(0, 0)
        print(hack.read())
        hack.close()
        print("Assembling Finished")
    else:
        errorMessage = "\033[0;31mERROR:\033[00m " + asmFile + " was not found in cwd"
        print(errorMessage)